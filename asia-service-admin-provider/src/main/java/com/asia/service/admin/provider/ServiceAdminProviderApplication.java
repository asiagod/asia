package com.asia.service.admin.provider;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * <p>
 *     DESCRIPTION:
 *          1.服务提供者
 *          2.Eureka客户端
 * </p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-29 9:46
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaClient
public class ServiceAdminProviderApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceAdminProviderApplication.class,args);
    }
}
