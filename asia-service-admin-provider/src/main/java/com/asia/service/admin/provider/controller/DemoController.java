package com.asia.service.admin.provider.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>DESCRIPTION:</p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-29 15:46
 **/
@RestController
public class DemoController {

    @Value("${spring.application.name}")
    private String name;

    @Value("${server.port}")
    private String port;

    @RequestMapping(value = "demo", method = RequestMethod.GET)
    public String demo() {
        return String.format("[ %s ] Test Success,Port: %s", name, port);
    }
}
