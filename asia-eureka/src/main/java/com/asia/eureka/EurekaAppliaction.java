package com.asia.eureka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * <p>DESCRIPTION:</p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-28 17:52
 **/
@SpringBootApplication
@EnableDiscoveryClient
@EnableEurekaServer
public class EurekaAppliaction {
    public static void main(String[] args) {
        SpringApplication.run(EurekaAppliaction.class,args);
    }
}
