package com.asia.service.admin.consumer.controller;

import com.asia.service.admin.consumer.service.FeginAdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>DESCRIPTION:</p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-29 15:59
 **/
@RestController
public class FeignAdminController {

    @Value("${spring.application.name}")
    private String name;

    @Autowired
    public FeginAdminService feginAdminService;

    @RequestMapping(value = "demo",method = RequestMethod.GET)
    public String demo(){
        String result = feginAdminService.demoService();
        return String.format(" [ %s ] ->"+result,name);
    }
}
