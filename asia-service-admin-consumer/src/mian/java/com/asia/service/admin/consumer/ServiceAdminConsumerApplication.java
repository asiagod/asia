package com.asia.service.admin.consumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * <p>DESCRIPTION:</p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-29 10:21
 **/
@SpringBootApplication
@EnableEurekaClient
@EnableDiscoveryClient
@EnableFeignClients
public class ServiceAdminConsumerApplication {
    public static void main(String[] args) {
        SpringApplication.run(ServiceAdminConsumerApplication.class,args);
    }
}
