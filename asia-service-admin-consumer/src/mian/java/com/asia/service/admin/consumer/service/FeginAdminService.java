package com.asia.service.admin.consumer.service;

import com.asia.service.admin.consumer.service.hystrix.AdminServiceHystrix;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "asia-service-admin-provider",fallback = AdminServiceHystrix.class)
public interface FeginAdminService {

    @RequestMapping(value = "demo",method = RequestMethod.GET)
    public String demoService();
}
