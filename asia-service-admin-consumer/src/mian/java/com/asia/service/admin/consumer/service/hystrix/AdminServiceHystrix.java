package com.asia.service.admin.consumer.service.hystrix;

import com.asia.service.admin.consumer.service.FeginAdminService;
import org.springframework.stereotype.Component;

/**
 * <p>
 *     DESCRIPTION:
 *       熔断器
 * </p>
 *
 * @author AsiaCui
 * @version: 1.0.0
 * @create 2019-01-29 16:54
 **/
@Component
public class AdminServiceHystrix implements FeginAdminService {
    @Override
    public String demoService() {

        return "调用熔断器";
    }
}
